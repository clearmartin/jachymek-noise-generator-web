CREATE DATABASE jachymek DEFAULT CHARACTER SET utf8;
GRANT SELECT ON jachymek.* to 'jachymek'@'localhost' IDENTIFIED BY 'jachymek';
GRANT INSERT ON jachymek.* to 'jachymek'@'localhost' IDENTIFIED BY 'jachymek';
GRANT UPDATE ON jachymek.* to 'jachymek'@'localhost' IDENTIFIED BY 'jachymek';
GRANT DELETE ON jachymek.* to 'jachymek'@'localhost' IDENTIFIED BY 'jachymek';

GRANT SELECT ON jachymek.* to 'jachymek_admin'@'localhost' IDENTIFIED BY 'jachymek_admin';
GRANT INSERT ON jachymek.* to 'jachymek_admin'@'localhost' IDENTIFIED BY 'jachymek_admin';
GRANT UPDATE ON jachymek.* to 'jachymek_admin'@'localhost' IDENTIFIED BY 'jachymek_admin';
GRANT DELETE ON jachymek.* to 'jachymek_admin'@'localhost' IDENTIFIED BY 'jachymek_admin';
GRANT CREATE ON jachymek.* to 'jachymek_admin'@'localhost' IDENTIFIED BY 'jachymek_admin';
GRANT DROP ON jachymek.* to 'jachymek_admin'@'localhost' IDENTIFIED BY 'jachymek_admin';

--DROP DATABASE jachymek;
--REVOKE ALL PRIVILEGES, GRANT OPTION FROM 'jachymek'@'localhost';
--DROP USER 'jachymek'@'localhost';
