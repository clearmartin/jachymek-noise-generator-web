CREATE TABLE shouts (
    id INT NOT NULL AUTO_INCREMENT,
    shout VARCHAR(10240) NOT NULL,
    picture_path VARCHAR(256) NOT NULL,
    created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
