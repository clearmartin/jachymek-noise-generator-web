// use sqlx::{MySqlPool};
// use sqlx::mysql::MySqlPoolOptions;
//
// // change for different database
// pub type DbPool = MySqlPool;
//
// pub async fn create_pool(count: u32) -> Result<DbPool, sqlx::Error> {
//
//     let database_url = std::env::var("DATABASE_URL").expect("DATABASE_URL is not set in .env file");
//
//     MySqlPoolOptions::new()
//         .max_connections(count)
//         .connect(&database_url).await
// }

use diesel::mysql::MysqlConnection;
//use diesel::prelude::*;
use diesel::r2d2::{ConnectionManager, Pool};

// pub fn establish_connection() -> MysqlConnection {
//     let database_url = std::env::var("DATABASE_URL").expect("DATABASE_URL must be set");
//
//     MysqlConnection::establish(&database_url)
//         .unwrap_or_else(|_| panic!("Error connecting to {}", database_url))
// }

pub type DbPool = Pool<ConnectionManager<MysqlConnection>>;

pub fn create_pool() -> DbPool {
    let database_url = std::env::var("DATABASE_URL").expect("DATABASE_URL must be set");

    let manager = ConnectionManager::<MysqlConnection>::new(database_url);
    // Refer to the `r2d2` documentation for more methods to use
    // when building a connection pool
    Pool::builder()
        .test_on_check_out(true)
        .build(manager)
        .expect("Could not build connection pool")
}
