// @generated automatically by Diesel CLI.

diesel::table! {
    shouts (id) {
        id -> Integer,
        shout -> Varchar,
        picture_path -> Varchar,
        created -> Datetime,
        modified -> Datetime,
    }
}
