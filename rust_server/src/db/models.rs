use serde::{Serialize};
use chrono::{NaiveDateTime};
use diesel;
use diesel::prelude::*;

use crate::db::DbPool;
use crate::db::schema::shouts;

#[derive(Queryable, Serialize, Clone, Insertable, AsChangeset)]
pub struct Shout {
    pub id: i32,
    pub shout: String,
    pub picture_path: String,
    #[serde(with = "to_offset_datetime")]
    pub created: NaiveDateTime,
    #[serde(with = "to_offset_datetime")]
    pub modified: NaiveDateTime,
}

mod to_offset_datetime {
    use chrono::{Local, NaiveDateTime, TimeZone};
    use serde::{Serialize, Serializer};

    pub fn serialize<S: Serializer>(time: &NaiveDateTime, serializer: S) -> Result<S::Ok, S::Error> {
        time_to_json(&time.clone()).serialize(serializer)
    }

    fn time_to_json(t: &NaiveDateTime) -> String {
        chrono_tz::Europe::Prague.from_local_datetime(t).unwrap().with_timezone(&Local).to_rfc3339()
    }
}

impl Shout {
    pub fn create(db_pool: &DbPool, shout: &String, picture_path: &String) -> QueryResult<Shout> {
        let mut connection = db_pool.get().unwrap();
        let inserted = diesel::insert_into(shouts::table)
            .values((
                shouts::shout.eq(&shout),
                shouts::picture_path.eq(&picture_path)
            ))
            .execute(&mut connection);
        match inserted {
            Ok(_) => Self::find_newest(db_pool),
            Err(err) => Err(err)
        }
    }

    pub fn list(db_pool: &DbPool) -> QueryResult<Vec<Shout>> {
        let mut connection = db_pool.get().unwrap();
        shouts::table.order(shouts::id).load::<Shout>(&mut connection)
    }

    pub fn find_newest(db_pool: &DbPool) -> QueryResult<Shout> {
        let mut connection = db_pool.get().unwrap();
        shouts::table.order(shouts::id.desc()).first(&mut connection)
    }

    pub fn update(db_pool: &DbPool, shout: &String, picture_path: &String, id: &i32) -> QueryResult<usize> {
        let mut connection = db_pool.get().unwrap();
        diesel::update(shouts::table.find(id))
            .set((
                shouts::shout.eq(shout),
                shouts::picture_path.eq(picture_path)
            ))
            .execute(&mut connection)
    }

    pub fn delete(db_pool: &DbPool, id: &i32) -> QueryResult<usize> {
        let mut connection = db_pool.get().unwrap();
        diesel::delete(shouts::table.find(id)).execute(&mut connection)
    }
}
