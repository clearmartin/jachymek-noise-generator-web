// mod model;
mod connection;
pub mod models;
pub mod schema;

// pub use model::*;
// pub use connection::create_pool;
// pub use connection::DbPool;
pub use connection::create_pool;
pub use connection::DbPool;