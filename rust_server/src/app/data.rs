use crate::db::DbPool;

pub struct AppData {
    pub pool: DbPool,
    pub web_password: String,
    pub static_files_path: String,
}