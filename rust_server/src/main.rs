use actix_session::config::CookieContentSecurity;
use actix_session::SessionMiddleware;
use actix_session::storage::CookieSessionStore;
use actix_web::{get, web, App, HttpResponse, HttpServer, Responder, cookie::SameSite, middleware::Logger, cookie::Key};
//use actix_service::Service;
//use futures::future::FutureExt;
use std::env;
use env_logger::Env;
use rand::Rng;

mod db;
mod app;
mod route;



#[get("/")]
async fn welcome() -> impl Responder {
    HttpResponse::Ok().insert_header(("Content-Type", "text/plain; charset=UTF-8")).body("Jáchymek Noise Generátor Server")
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {

    //let db_pool = db::create_pool(2).await.unwrap();
    let db_pool = db::create_pool();

    let host = env::var("BIND_HOST").expect("BIND_HOST is not set in .env file");
    let port = env::var("BIND_PORT").expect("BIND_PORT is not set in .env file");

    let photo_password = env::var("WEB_PASSWORD").expect("WEB_PASSWORD is not set in .env file");
    let static_files_path = env::var("STATIC_FILES_PATH").expect("STATIC_FILES_PATH is not set in .env file");

    env_logger::Builder::from_env(Env::default().default_filter_or("debug")).init();

    let private_key = rand::thread_rng().gen::<[u8; 32]>();

    let server = HttpServer::new(move || {
        App::new()
            .wrap(Logger::default())
            //.wrap(DefaultHeaders::new().header("Cache-Control", "no-store"))
            .wrap(
                SessionMiddleware::builder(CookieSessionStore::default(), Key::derive_from( &private_key))
                    .cookie_content_security(CookieContentSecurity::Private)
                    .cookie_same_site(SameSite::Strict)
                    .build() // <- create cookie based session middleware
            )
            .app_data(web::Data::new(app::AppData {
                pool: db_pool.clone(),
                web_password: photo_password.clone(),
                static_files_path: static_files_path.clone(),
            })) // pass database pool to application so we can access it inside handlers
            .service(welcome)
            .configure(route::service_init)
    });
    server.bind(format!("{}:{}", host, port))?
        .run()
        .await
}
