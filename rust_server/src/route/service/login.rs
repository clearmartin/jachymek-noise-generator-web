use actix_session::Session;
use actix_web::{get, HttpResponse, post, Responder, web};
use serde::Deserialize;
use crate::app::AppData;

#[derive(Deserialize)]
pub(crate) struct LoginData {
    password: String,
}

pub fn is_logged_in(session: Session) -> bool {
    match session.get::<bool>("logged_in") {
        Ok(res) => {
            log::debug!("is_logged_in res is none: {}", res.is_none());
            match res {
                Some(true) => true,
                _ => false,
            }
        },
        _ => false,
    }
}

#[get("/login")]
pub(crate) async fn get_login_state(session: Session) -> impl Responder {

    log::debug!("get_login_state");

    if is_logged_in(session) {
        HttpResponse::NoContent()
    } else {
        HttpResponse::Unauthorized()
    }
}

#[post("/login")]
pub(crate) async fn log_in(session: Session, data: web::Data<AppData>, query: web::Json<LoginData>) -> impl Responder {

    log::debug!("login");

    let matched = query.password == data.get_ref().web_password;
    match session.insert("logged_in", matched) {
        Ok(_) => {
            log::debug!("logged_in: {}", session.get::<bool>("logged_in").unwrap().unwrap());

            if matched {
                HttpResponse::Ok()
            } else {
                HttpResponse::Unauthorized()
            }
        }
        Err(_) => HttpResponse::Unauthorized()
    }
}
