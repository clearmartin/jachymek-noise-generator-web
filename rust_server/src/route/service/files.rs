use std::path::PathBuf;
use actix_files::NamedFile;
use actix_session::Session;
use actix_web::{Either, get, HttpRequest, HttpResponse, Responder, web};
use actix_web::http::StatusCode;
use crate::app::AppData;
use crate::route::service::login;


#[get("/static/{filename:.*}")]
pub(crate) async fn get_static_file(session: Session, data: web::Data<AppData>, req: HttpRequest) -> Either<impl Responder, HttpResponse> {

    log::debug!("get_static_file");

    if login::is_logged_in(session) {
        let mut path = PathBuf::from(&data.get_ref().static_files_path);

        let filename: PathBuf = req.match_info().query("filename").parse().unwrap();
        path.push(filename.to_str().unwrap());

        log::debug!("requested file: {}", filename.to_str().unwrap());

        match NamedFile::open(path) {
            Ok(file) => {

                let res = file
                    .use_last_modified(true)
                    .disable_content_disposition();

                if filename.to_str().unwrap() == "manifest.json" {
                    Either::Left(res.customize().with_status(StatusCode::OK))
                } else {
                    Either::Left(res.customize().insert_header(("Cache-Control", "private")))
                }
            }
            _ => Either::Right(HttpResponse::NotFound().finish())
        }

    } else {
        Either::Right(HttpResponse::Unauthorized().finish())
    }
}
