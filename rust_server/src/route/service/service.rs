use actix_web::middleware::DefaultHeaders;
use actix_web::web;
use crate::route::service::{files, login, shout};

pub fn init(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::scope("/service")
            .wrap(DefaultHeaders::new().add(("Cache-Control", "no-store")))
            .service(login::get_login_state)
            .service(login::log_in)

            .service(files::get_static_file)

            .service(shout::get_newest_shout)
            .service(shout::get_shouts)
            .service(shout::delete_shout)
            .service(shout::update_shout)
            .service(shout::create_shout)
    );
}