use actix_session::Session;
use actix_web::{get, HttpResponse, post, delete, put, Responder, web};
use serde::Deserialize;

use crate::app::AppData;
use crate::db::models::*;
use crate::route::service::login;

#[derive(Deserialize)]
pub(crate) struct DeleteShoutQuery {
    id: i32,
}

#[derive(Deserialize)]
pub(crate) struct UpdateShoutQuery {
    shout: String,
    picture_path: String,
    id: i32,
}

#[derive(Deserialize)]
pub(crate) struct CreateShoutQuery {
    shout: String,
    picture_path: String,
}

#[get("/shout/latest")]
pub(crate) async fn get_newest_shout(session: Session, data: web::Data<AppData>) -> impl Responder {
    if !login::is_logged_in(session) {
        return HttpResponse::Unauthorized().body("unauthorized");
    }
    let shout = Shout::find_newest(&data.get_ref().pool);
    match shout {
        Ok(res) => HttpResponse::Ok().json(res),
        Err(err) => match err {
            diesel::result::Error::DatabaseError(_, info) => HttpResponse::InternalServerError().body(info.message().to_string()),
            diesel::result::Error::NotFound => HttpResponse::NotFound().finish(),
            _ => HttpResponse::InternalServerError().body("other_error"),
        },
    }
}

#[get("/shout/list")]
pub(crate) async fn get_shouts(session: Session, data: web::Data<AppData>) -> impl Responder {
    if !login::is_logged_in(session) {
        return HttpResponse::Unauthorized().body("unauthorized");
    }

    let result = Shout::list(&data.get_ref().pool);
    match result {
        Ok(shouts) => HttpResponse::Ok().json(shouts),
        Err(_) => HttpResponse::InternalServerError().body("db_error"),
    }
}

#[delete("/shout")]
pub(crate) async fn delete_shout(session: Session, data: web::Data<AppData>, query: web::Query<DeleteShoutQuery>) -> impl Responder {
    if !login::is_logged_in(session) {
        return HttpResponse::Unauthorized().body("unauthorized");
    }
    let result = Shout::delete(&data.get_ref().pool, &query.id);
    match result {
        Ok(_) => HttpResponse::Ok().body("success"),
        Err(_) => HttpResponse::NotFound().body("not_exist"),
    }
}

#[put("/shout")]
pub(crate) async fn update_shout(session: Session, data: web::Data<AppData>, query: web::Json<UpdateShoutQuery>) -> impl Responder {
    if !login::is_logged_in(session) {
        return HttpResponse::Unauthorized().body("unauthorized");
    }
    let result = Shout::update(&data.get_ref().pool, &query.shout, &query.picture_path, &query.id);
    match result {
        Ok(_) => HttpResponse::Ok().body("success"),
        Err(_) => HttpResponse::NotFound().body("not_exist"),
    }
}

#[post("/shout")]
pub(crate) async fn create_shout(session: Session, data: web::Data<AppData>, query: web::Json<CreateShoutQuery>) -> impl Responder {
    if !login::is_logged_in(session) {
        return HttpResponse::Unauthorized().body("unauthorized");
    }
    let result = Shout::create(&data.get_ref().pool, &query.shout, &query.picture_path);
    match result {
        Ok(res) => HttpResponse::Ok().json(res),
        Err(_) => HttpResponse::InternalServerError().body("db_error")
    }
}
