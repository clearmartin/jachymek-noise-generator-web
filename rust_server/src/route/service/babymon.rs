use actix_session::Session;
use actix_web::{get, HttpResponse, post, delete, put, Responder, web};
use serde::Deserialize;

use crate::app::AppData;
use crate::db::models::*;
use crate::route::service::login;

//TODO

#[get("/babymon/environment_data")]
pub(crate) async fn get_environment_data(session: Session, data: web::Data<AppData>) -> impl Responder {
    if !login::is_logged_in(session) {
        return HttpResponse::Unauthorized().body("unauthorized");
    }
    let shout = Shout::find_newest(&data.get_ref().pool);
    match shout {
        Ok(res) => HttpResponse::Ok().json(res),
        Err(err) => match err {
            diesel::result::Error::DatabaseError(_, info) => HttpResponse::InternalServerError().body(info.message().to_string()),
            diesel::result::Error::NotFound => HttpResponse::NotFound().finish(),
            _ => HttpResponse::InternalServerError().body("other_error"),
        },
    }
}
