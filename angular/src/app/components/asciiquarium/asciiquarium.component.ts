import { Component } from '@angular/core';

@Component({
    selector: 'app-asciiquarium',
    templateUrl: './asciiquarium.component.html',
    styleUrls: ['./asciiquarium.component.scss']
})
export class AsciiquariumComponent {

    static NUMBER_OF_FISH = 2;
    static STEP = 7;
    static MAX_LEFT = 300;
    static MIN_LEFT = -50;

    fishLeft: number[] = [];
    fishLeftCss: string[] = [];

    ngAfterViewInit() {
        this.scheduleFish();
    }

    scheduleFish(): void {
        for (let i = 0; i < AsciiquariumComponent.NUMBER_OF_FISH; i++) {

            // init each fish
            const fishLeft = i === 0 ? 300 : 400;
            this.fishLeft.push(fishLeft);
            this.fishLeftCss.push(`${fishLeft}px`);
            const speed = i === 0 ? 900 : 600;

            // schedule each fish
            setInterval(() => {
                let fishLeft = this.fishLeft[i];
                if (fishLeft <= AsciiquariumComponent.MIN_LEFT) {
                    fishLeft = AsciiquariumComponent.MAX_LEFT + AsciiquariumComponent.STEP;
                }
                fishLeft = fishLeft - AsciiquariumComponent.STEP;

                this.fishLeft[i] = fishLeft;
                this.fishLeftCss[i] = `${fishLeft}px`;
            }, speed);
        }
    }

}
