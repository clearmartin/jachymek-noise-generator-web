import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Util } from 'src/app/util/util';
import { LoginState, StateService } from 'src/app/service/state.service';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    asciiartHtml: string = '';
    asciiartHtmlColour: SafeHtml | null = null;
    password: string = '';
    wrapperClass: string = '';

    @ViewChild('passwordInput') passwordInput?: ElementRef;

    constructor(private http: HttpClient, private state: StateService, private domSanitizer: DomSanitizer) { }

    ngOnInit(): void {
    }

    ngAfterViewInit() {
        this.loadBackground();
    }

    async loadBackground() {
        this.asciiartHtml = await Util.getAsciiArtHtml('/assets/asciiart/login_background.html', this.http);
        this.asciiartHtmlColour = this.domSanitizer.bypassSecurityTrustHtml(this.asciiartHtml);
        this.passwordInput?.nativeElement.focus();
    }

    submitPassword(): void {

        this.http.post('/service/login', { password: this.password }).subscribe({
            next: (res: any) => {
                console.info('login success');
                this.state.loginState.next(LoginState.LOGGED_IN);
            },
            error: (err: any) => {
                console.error('login error TODO');
                this.state.loginState.next(LoginState.ERROR);
            }
        });
    }

    keyTyped(): void {
        const aRand: string = Util.randomNumStr(12);
        const bRand: string = Util.randomNumStr(4);
        this.wrapperClass = `noise-a-${aRand} noise-b-${bRand}`;
    }

}
