import { Component, OnInit, ElementRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StateService } from '../../service/state.service';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { Util } from 'src/app/util/util';

enum Direction {
    NEXT,
    PREV,
}

interface Shout {
    id: number;
    shout: string;
    picture_path: string;
    created: Date;
    modified: Date;
}

enum Mode {
    DISPLAYING_LATEST = 'DISPLAYING_LATEST',
    DISPLAYING_ALL = 'DISPLAYING_ALL',
    CREATING = 'CREATING',
    EDITING = 'EDITING',
}

enum PinguState {
    RIGHT = 1,
    MIDDLE_RIGHT = 2,
    MIDDLE = 3,
    MIDDLE_LEFT = 4,
    LEFT = 5,
}

enum LatestImageMode {
    COLOUR = 1,
    NOCOLOUR = 2,
    REAL = 3,
}

@Component({
    selector: 'app-shout-pane',
    templateUrl: './shout-pane.component.html',
    styleUrls: ['./shout-pane.component.scss'],
    encapsulation: ViewEncapsulation.None,
    preserveWhitespaces: true,
})
export class ShoutPaneComponent implements OnInit {

    static WITH_NOISE_CLASS: string = 'with-noise';
    static KEY_TYPED_CHANGE_EVER: number = 4;

    eMode = Mode;
    mode: Mode = Mode.DISPLAYING_LATEST;

    availablePicturePaths: string[] = [];

    keyTypedCounter: number = 0;

    asciiartHtmlLatest: string = '';
    asciiartHtmlColourLatest: SafeHtml | null = null;
    latestImageWidth: string | null = null;
    latestImageHeight: string | null = null;

    asciiartHtmlCreating: string = '';
    asciiartHtmlColourCreating: SafeHtml | null = null;

    pingu: string = '';
    pinguMirrored: boolean = false;
    pinguSide: string = '';
    pinguTurning: string = '';
    pinguForehead: string = '';
    pinguTurnTimeout: any = null;
    pinguState: PinguState = PinguState.RIGHT;

    wrapperClass: string = '';

    latestShout: Shout | null = null;
    latestShoutRealPicturePath: string = '';
    latestImageTimeout: any = null;
    latestImageMode: LatestImageMode = LatestImageMode.COLOUR;

    newShoutText: string = '';
    newShoutPicturePath: string = '';
    submittingShout: boolean = false;

    saveError: boolean = false;
    saveSuccess: boolean = false;

    @ViewChild('textareaInput') textareaInput?: ElementRef;
    @ViewChild('latestAsciiart') latestAsciiart?: ElementRef;

    textareaNoiseTimeout: any = null;
    _timeToNoiseMs: number = -1;
    timeToNoiseStr: string = '';
    timeToNoiseInterval: any = null;

    constructor(private http: HttpClient, private state: StateService, private domSanitizer: DomSanitizer) {
    }

    ngOnInit(): void {
    }

    ngAfterViewInit() {
        this.loadInitialData();
        this.loadLatest();
    }

    set timeToNoiseMs(value: number) {
        this._timeToNoiseMs = value;
        this.timeToNoiseStr = String(value / 1000).padStart(2, '0');
    }

    get timeToNoiseMs() {
        return this._timeToNoiseMs;
    }

    async loadInitialData() {
        this.availablePicturePaths = await Util.getAvailablePictures(this.http);

        this.pinguSide = await Util.getAsciiArtText('/assets/asciiart/pingu_side.txt', this.http);
        this.pinguTurning = await Util.getAsciiArtText('/assets/asciiart/pingu_turning.txt', this.http);
        this.pinguForehead = await Util.getAsciiArtText('/assets/asciiart/pingu_forehead.txt', this.http);
        this.pingu = this.pinguSide;
        this.schedulePinguTurning();
    }

    async loadLatest() {
        console.debug('loading latest');
        this.http.get<Shout>('/service/shout/latest').subscribe(async (res: Shout) => {
            this.latestShout = res;
            this.latestShoutRealPicturePath = '/service/static/' + res.picture_path.replace('.html', '.avif');

            this.mode = Mode.DISPLAYING_LATEST;

            this.asciiartHtmlLatest = await Util.getAsciiArtHtml(this.latestShout.picture_path, this.http);
            this.asciiartHtmlColourLatest = this.domSanitizer.bypassSecurityTrustHtml(this.asciiartHtmlLatest);

            this.latestImageWidth = null;
            this.latestImageHeight = null;

            setTimeout(() => {
                const preElem = this.latestAsciiart?.nativeElement.querySelector('pre');
                console.debug('preElem:', preElem);
                this.latestImageWidth = preElem.clientWidth + 'px';
                this.latestImageHeight = preElem.clientHeight + 'px';
                console.debug('latestImageWidth:' + this.latestImageWidth);
                console.debug('latestImageHeight:' + this.latestImageHeight);
                this.scheduleLatestImageChange();
            }, 1000);
        });
    }

    unscheduleLatestImageChange() {
        clearTimeout(this.latestImageTimeout);
    }

    scheduleLatestImageChange() {
        this.unscheduleLatestImageChange();
        console.debug('scheduleLatestImageChange');
        this.latestImageTimeout = setTimeout(() => {

            this.wrapperClass = Util.randomNextInt(20) === 1 ? 'latest-image-nocolour' : 'latest-image-real';

            const keepTimeout = this.wrapperClass === 'latest-image-real' ? 5000 : 2000;

            setTimeout(() => {
                this.wrapperClass = 'latest-image-colour';

                this.scheduleLatestImageChange();
            }, keepTimeout);

        }, Util.randomNextInt(10) * 1000);
    }

    createShout(): void {

        if (this.newShoutText == null || this.newShoutText.trim().length === 0) {
            this.saveError = true;
            setTimeout(() => {
                this.saveError = false;
            }, 1000);
            return;
        }

        this.saveError = false;
        this.submittingShout = true;

        this.http.post('/service/shout', { shout: this.newShoutText, picture_path: this.newShoutPicturePath }).subscribe({
            next: (res: any) => {
                this.saveSuccess = true;

                setTimeout(() => {
                    this.saveSuccess = false;
                    this.submittingShout = false;
                    this.loadLatest();
                }, 500);
            },
            error: (err: any) => {
                this.saveError = true;
                this.submittingShout = false;
            }
        });
    }

    updateShout(): void {

        this.saveError = false;

        this.http.put('/service/shout', { shout: this.newShoutText, picture_path: this.newShoutPicturePath, id: this.latestShout?.id }).subscribe({
            next: (res: any) => {
                this.saveSuccess = true;
            },
            error: (err: any) => {
                this.saveError = true;
            }
        });
    }

    keyTyped(): void {
        this.keyTypedCounter = (this.keyTypedCounter + 1) % ShoutPaneComponent.KEY_TYPED_CHANGE_EVER;
        console.debug('keyTypedCounter', this.keyTypedCounter);
        if (this.keyTypedCounter === 0) {
            // change picture
            this.changeTypingPicture();
        }
        //TODO!!
        const aRand: string = Util.randomNumStr(12);
        const bRand: string = Util.randomNumStr(4);
        this.wrapperClass = `noise-a-${aRand} noise-b-${bRand}`;
    }

    async changeTypingPicture() {
        const manifestIndex = Util.randomNextInt(this.availablePicturePaths.length);
        const picturePath = this.availablePicturePaths[manifestIndex].replace('.avif', '.html');
        this.newShoutPicturePath = picturePath;
        this.asciiartHtmlCreating = await Util.getAsciiArtHtml(this.newShoutPicturePath, this.http);
        this.asciiartHtmlColourCreating = this.domSanitizer.bypassSecurityTrustHtml(this.asciiartHtmlCreating);
    }

    switchCreatingMode(): void {
        this.mode = this.mode === Mode.DISPLAYING_LATEST ? Mode.CREATING : Mode.DISPLAYING_LATEST;
        if (this.mode === Mode.CREATING) {
            setTimeout(() => {
                this.textareaInput?.nativeElement.focus();
            }, 10);
            this.scheduleTextareaNoise();
            this.changeTypingPicture();
        } else {
            this.unscheduleTextareaNoise();
        }
    }

    unscheduleTextareaNoise(): void {
        this.timeToNoiseMs = 0;
        clearInterval(this.timeToNoiseInterval);
        clearTimeout(this.textareaNoiseTimeout);
    }

    scheduleTextareaNoise(): void {
        this.unscheduleTextareaNoise();
        this.timeToNoiseMs = Util.randomNextInt(100) * 1000;
        this.textareaNoiseTimeout = setTimeout(() => {
            this.textareaInput?.nativeElement.classList.add(ShoutPaneComponent.WITH_NOISE_CLASS);
            setTimeout(() => {
                this.textareaInput?.nativeElement.classList.remove(ShoutPaneComponent.WITH_NOISE_CLASS);
                this.scheduleTextareaNoise();
            }, 500 + Util.randomNextInt(15) * 100)
        }, this.timeToNoiseMs);

        console.debug('this.timeToNoiseMs:', this.timeToNoiseMs);

        this.timeToNoiseInterval = setInterval(() => {
            this.timeToNoiseMs -= 1000;
            if (this.timeToNoiseMs <= 0) {
                clearInterval(this.timeToNoiseInterval);
            }
        }, 1000);
    }

    schedulePinguTurning(): void {
        clearTimeout(this.pinguTurnTimeout);
        this.pinguTurnTimeout = setTimeout(() => {
            this.pinguState = this.getNextPinguState();
            switch (this.pinguState) {
                case PinguState.RIGHT:
                    this.pingu = this.pinguSide;
                    this.pinguMirrored =  false;
                    break;
                case PinguState.MIDDLE_RIGHT:
                    this.pingu = this.pinguTurning;
                    this.pinguMirrored =  false;
                    break;
                case PinguState.MIDDLE:
                    this.pingu = this.pinguForehead;
                    this.pinguMirrored =  false;
                    break;
                case PinguState.MIDDLE_LEFT:
                    this.pingu = this.pinguTurning;
                    this.pinguMirrored =  true;
                    break;
                case PinguState.LEFT:
                default:
                    this.pingu = this.pinguSide;
                    this.pinguMirrored =  true;
            }
            this.schedulePinguTurning();
        }, 4000);
    }

    getNextPinguState(): PinguState {
        return 1 + Util.randomNextInt(5);
    }

}
