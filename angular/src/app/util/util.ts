import { HttpClient } from "@angular/common/http";

export class Util {

    static async getAsciiArtHtml(path: string, http: HttpClient): Promise<string> {
        return this.getAsciiArtText(path, http, (res: string) => {
            const htmlBody: HTMLElement = new DOMParser().parseFromString(res, 'text/html').body;
            return htmlBody.innerHTML;
        });
    }

    static async getAvailablePictures(http: HttpClient): Promise<string[]> {
        const jsonString = await this.getAsciiArtText('manifest.json', http);
        return (JSON.parse(jsonString) as string[]);
    }

    static async getAsciiArtText(path: string, http: HttpClient, processResponse: (res: string) => string = (res) => res): Promise<string> {
        let url = path;
        if (!path.startsWith('/assets')) {
            url = `/service/static/${path}`;
        }
        return new Promise((resolve, reject) => {
            http.get(url, { responseType: 'text' }).subscribe({
                next: (res: string) => {
                    resolve(processResponse(res));
                },
                error: (err: any) => {
                    reject(err);
                }
            });
        });
    }

    static randomNextInt(max: number): number {
        return Math.floor(Math.random() * max);
    }

    static randomNumStr(max: number, padZerosSize: number = 2): string {
        return String(this.randomNextInt(max)).padStart(padZerosSize, '0');
    }

}