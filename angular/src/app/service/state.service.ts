import { Injectable, EventEmitter } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export enum LoginState {
    INIT = 'init',
    LOGGED_OUT = 'logged_out',
    LOGGED_IN = 'logged_in',
    ERROR = 'error',
}

@Injectable()
export class StateService {

    public loginState: BehaviorSubject<LoginState> = new BehaviorSubject<LoginState>(LoginState.INIT);

    public swipeLeft: EventEmitter<any> = new EventEmitter();
    public swipeRight: EventEmitter<any> = new EventEmitter();

    constructor () {}

}
