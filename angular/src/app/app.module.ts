import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

import { StateService } from './service/state.service';

import { LoginComponent } from './components/login/login.component';
import { ShoutPaneComponent } from './components/shout-pane/shout-pane.component';
import { AsciiquariumComponent } from './components/asciiquarium/asciiquarium.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ShoutPaneComponent,
    AsciiquariumComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [
    StateService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
