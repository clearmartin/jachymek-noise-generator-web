import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { LoginState, StateService } from './service/state.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
    showLogin: boolean = false;
    showShouts: boolean = false;

    private swipeCoord?: [number, number];
    private swipeTime?: number;

    constructor(private elementRef: ElementRef, private detectorRef: ChangeDetectorRef, private state: StateService, private http: HttpClient) {}

    ngOnInit() {
        this.state.loginState.subscribe((loginState) => {
            if (loginState === LoginState.INIT) {
                this.showLogin = false;
                this.showShouts = false;
            } else if (loginState === LoginState.LOGGED_OUT) {
                this.showLogin = true;
                this.showShouts = false;
            } else if (loginState === LoginState.LOGGED_IN) {
                this.showLogin = false;
                this.showShouts = true;
            }
        });
    }

    ngAfterViewInit() {
        this.loadLoginState();
    }

    loadLoginState() {
        this.http.get('/service/login').subscribe({
            next: (res: any) => {
                this.state.loginState.next(LoginState.LOGGED_IN);
            },
            error: (err: any) => {
                this.state.loginState.next(LoginState.LOGGED_OUT);
            }
        });
    }

    swipe(e: TouchEvent, when: string): void {
        const coord: [number, number] = [e.changedTouches[0].clientX, e.changedTouches[0].clientY];
        const time = new Date().getTime();

        if (when === 'start') {
            this.swipeCoord = coord;
            this.swipeTime = time;
        } else if (when === 'end') {
            if (this.swipeCoord == null || this.swipeTime == null) {
                return;
            }
            const direction = [coord[0] - this.swipeCoord[0], coord[1] - this.swipeCoord[1]];
            const duration = time - this.swipeTime;

            if (duration < 1000
                && Math.abs(direction[0]) > 30 // Long enough
                && Math.abs(direction[0]) > Math.abs(direction[1] * 3)) { // Horizontal enough

                const swipe = direction[0] < 0 ? 'next' : 'previous';
                if (swipe === 'next') {
                    this.state.swipeRight.emit();
                } else {
                    this.state.swipeLeft.emit();
                }
            }
        }
    }
}
