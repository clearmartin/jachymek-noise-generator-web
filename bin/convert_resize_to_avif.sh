#!/bin/bash

out_folder="out"
rm -rf ${out_folder}/
mkdir ${out_folder}/

for f in `find . -maxdepth 1 -type f -printf '%P\n' | sort`; do
    echo "$f"
    base_f="${f%.*}"
    target_image1="${out_folder}/${base_f}.png"
    target_image2="${out_folder}/${base_f}.avif"
    convert "$f" -resize "1920x>" "${target_image1}"
    time cavif "${target_image1}" -o "${target_image2}"
    rm "${target_image1}"
done
