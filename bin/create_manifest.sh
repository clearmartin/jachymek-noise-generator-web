#!/bin/bash

{
  echo "["
  for f in `find . -type f | sort | rg "(.webp|.avif)$"`; do
    ff=`echo "$f" | cut -c 3-`
    echo "  \"$ff\","
  done
  echo "]"
} | sd ",\n]" "\n]" > manifest.json
