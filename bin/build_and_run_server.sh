#!/bin/bash

cd "$( dirname "${BASH_SOURCE[0]}" )"
cd ..
BASE=`pwd`

cd $BASE/rust_server
cargo build --release

cd $BASE/angular
npm run build
mv $BASE/angular/dist/jachymeknoisegenerator_final $BASE/angular/dist/jachymeknoisegenerator_final.trash
mv $BASE/angular/dist/jachymeknoisegenerator $BASE/angular/dist/jachymeknoisegenerator_final
rm -rf $BASE/angular/dist/jachymeknoisegenerator_final.trash

cd $BASE/rust_server
./start_nohup.sh

echo "build script finished"
