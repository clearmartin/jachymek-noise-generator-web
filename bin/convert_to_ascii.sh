#!/bin/bash

out_folder="ascii"
rm -rf ${out_folder}/
mkdir ${out_folder}/

for f in `find . -maxdepth 1 -type f -printf '%P\n' | sort`; do
    echo "$f"
    base_f="${f%.*}"
    temp_f="${out_folder}/${base_f}.png"
    target_f="${out_folder}/${base_f}.html"
    convert "$f" "${temp_f}"
    artem "${temp_f}" --size 500 --output "${target_f}"
    rm "${temp_f}"
done
